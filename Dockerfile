FROM alpine:3.12

# Установка необходимых утилит
RUN apk add --no-cache bash curl unzip

# Установка Terraform
ENV TERRAFORM_VERSION=1.0.0
RUN curl -LO https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
    unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
    mv terraform /usr/local/bin/ && \
    rm terraform_${TERRAFORM_VERSION}_linux_amd64.zip

# Установка AWS CLI (опционально, если нужно)
RUN apk add --no-cache python3 py3-pip && pip3 install awscli

CMD ["bash"]

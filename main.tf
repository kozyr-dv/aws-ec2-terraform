provider "aws" {
  region = "eu-central-1"  # Укажите ваш регион AWS
}

resource "aws_instance" "example" {
  count         = 2
  ami           = "ami-026c3177c9bd54288"  # Укажите правильный AMI ID для вашего региона
  instance_type = "t2.micro"

  tags = {
    Name = "TFCreateInstance-${count.index}"
  }
}